//
//  GameScene.h
//  PompaDroid
//
//  Created by John Watson on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#import "GameLayer.h"
#import "HudLayer.h"

@interface GameScene : CCScene {
        
}

@property (nonatomic, unsafe_unretained) GameLayer *gameLayer;
@property (nonatomic, unsafe_unretained) HudLayer *hudLayer;

-(void)endGame;

@end
