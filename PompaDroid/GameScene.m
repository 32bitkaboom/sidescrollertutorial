//
//  GameScene.m
//  PompaDroid
//
//  Created by John Watson on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene

@synthesize gameLayer = _gameLayer;
@synthesize hudLayer  = _hudLayer;

-(id)init
{
    if( self = [super init] )
    {
        self.gameLayer = [GameLayer node];
        [self addChild:self.gameLayer z:0];
        
        self.hudLayer = [HudLayer node];
        [self addChild:self.hudLayer z:1];
        
        self.hudLayer.dPad.delegate = self.gameLayer;
    }
    
    return self;
}

-(void)endGame {
    CCLabelTTF *restartLabel = [CCLabelTTF labelWithString:@"RESTART" fontName:@"Arial" fontSize:30];
    CCMenuItemLabel *restartItem = [CCMenuItemLabel itemWithLabel:restartLabel target:self selector:@selector(restartGame)];
    CCMenu *menu = [CCMenu menuWithItems:restartItem, nil];
    menu.position = CENTER;
    menu.tag = 5;
    [self.hudLayer addChild:menu z:5];
}

-(void)restartGame {
    [[CCDirector sharedDirector] replaceScene:[GameScene node]];
}

@end

