//
//  Hero.m
//  PompaDroid
//
//  Created by John Watson on 12/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Hero.h"

@implementation Hero

-(id) init {
    if( (self = [super initWithSpriteFrameName:@"hero_idle_00.png"]) )
    {
        int i;
        
        //=== IDLE ANIMATION ===//
        CCArray *idleFrames = [CCArray arrayWithCapacity:6];
        for( i=0; i < 6; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"hero_idle_%02d.png", i]];
            [idleFrames addObject:frame];
        }
        CCAnimation *idleAnimation = [CCAnimation animationWithSpriteFrames:[idleFrames getNSArray] delay:1.0/12.0];
        self.idleAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:idleAnimation]];
        
        
        //=== ATTACK ANIMATION ===//
        CCArray *attackFrames = [CCArray arrayWithCapacity:3];
        for( i=0; i < 3; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"hero_attack_00_%02d.png", i]];
            [attackFrames addObject:frame];
        }
        CCAnimation *attackAnimation = [CCAnimation animationWithSpriteFrames:[attackFrames getNSArray] delay:1.0/24.0];
        self.attackAction = [CCSequence actions:[CCAnimate actionWithAnimation:attackAnimation], [CCCallFunc actionWithTarget:self selector:@selector(idle)], nil];
        
        //=== WALK ANIMATION ===//
        CCArray *walkFrames = [CCArray arrayWithCapacity:8];
        for( i=0; i < 8; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"hero_walk_%02d.png", i]];
            [walkFrames addObject:frame];
        }
        CCAnimation *walkAnimation = [CCAnimation animationWithSpriteFrames:[walkFrames getNSArray] delay:1.0/24.0];
        self.walkAction = [CCSequence actions:[CCAnimate actionWithAnimation:walkAnimation], [CCCallFunc actionWithTarget:self selector:@selector(idle)], nil];

        //=== HURT ANIMATION ===//
        CCArray *hurtFrames = [CCArray arrayWithCapacity:3];
        for( i=0; i < 3; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"hero_hurt_%02d.png", i]];
            [hurtFrames addObject:frame];
        }
        CCAnimation *hurtAnimation = [CCAnimation animationWithSpriteFrames:[hurtFrames getNSArray] delay:1.0/24.0];
        self.hurtAction = [CCSequence actions:[CCAnimate actionWithAnimation:hurtAnimation], [CCCallFunc actionWithTarget:self selector:@selector(idle)], nil];

        //=== KNOCKOUT ANIMATION ===//
        CCArray *knockoutFrames = [CCArray arrayWithCapacity:5];
        for( i=0; i < 5; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"hero_knockout_%02d.png", i]];
            [knockoutFrames addObject:frame];
        }
        CCAnimation *knockoutAnimation = [CCAnimation animationWithSpriteFrames:[knockoutFrames getNSArray] delay:1.0/24.0];
        self.knockedOutAction = [CCSequence actions:[CCAnimate actionWithAnimation:knockoutAnimation], [CCBlink actionWithDuration:2.0f blinks:10.0], [CCCallFunc actionWithTarget:self selector:@selector(dead)], nil];
    }
    
    self.centerToBottom = 39.0;
    self.centerToSides = 29.0;
    self.hitPoints = 100.0;
    self.damage = 20.0;
    self.walkSpeed = 80;
    
    self.hitBox = [self createBoundingBoxWithOrigin:ccp(-self.centerToSides, -self.centerToBottom) size:CGSizeMake(self.centerToSides * 2, self.centerToBottom * 2)];
    self.attackBox = [self createBoundingBoxWithOrigin:ccp(self.centerToSides, -10) size:CGSizeMake(20, 20)];
    
    return self;
}


@end
