//
//  Robot.h
//  PompaDroid
//
//  Created by John Watson on 9/29/13.
//
//

#import "ActionSprite.h"

@class Hero;

@interface Robot : ActionSprite
{
    
}

@property CGFloat nextDecisionTime;

- (void) makeDecisionWithHero:(Hero *)hero andIsOnScreen:(BOOL)isOnScreen;
    
@end
