//
//  Robot.m
//  PompaDroid
//
//  Created by John Watson on 9/29/13.
//
//

#import "Robot.h"
#import "Hero.h"

@implementation Robot

-(id) init
{
    if( (self = [super initWithSpriteFrameName:@"robot_idle_00.png"]) )
    {
        int i;
        
        //=== IDLE ANIMATION ===//
        CCArray *idleFrames = [CCArray arrayWithCapacity:5];
        for( i=0; i < 5; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"robot_idle_%02d.png", i]];
            [idleFrames addObject:frame];
        }
        CCAnimation *idleAnimation = [CCAnimation animationWithSpriteFrames:[idleFrames getNSArray] delay:1.0/12.0];
        self.idleAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:idleAnimation]];
        
        
        //=== ATTACK ANIMATION ===//
        CCArray *attackFrames = [CCArray arrayWithCapacity:5];
        for( i=0; i < 5; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"robot_attack_%02d.png", i]];
            [attackFrames addObject:frame];
        }
        CCAnimation *attackAnimation = [CCAnimation animationWithSpriteFrames:[attackFrames getNSArray] delay:1.0/24.0];
        self.attackAction = [CCSequence actions:[CCAnimate actionWithAnimation:attackAnimation], [CCCallFunc actionWithTarget:self selector:@selector(idle)], nil];
        
        //=== WALK ANIMATION ===//
        CCArray *walkFrames = [CCArray arrayWithCapacity:6];
        for( i=0; i < 6; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"robot_walk_%02d.png", i]];
            [walkFrames addObject:frame];
        }
        CCAnimation *walkAnimation = [CCAnimation animationWithSpriteFrames:[walkFrames getNSArray] delay:1.0/24.0];
        self.walkAction = [CCSequence actions:[CCAnimate actionWithAnimation:walkAnimation], [CCCallFunc actionWithTarget:self selector:@selector(idle)], nil];
        
        //=== HURT ANIMATION ===//
        CCArray *hurtFrames = [CCArray arrayWithCapacity:6];
        for( i=0; i < 3; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"robot_hurt_%02d.png", i]];
            [hurtFrames addObject:frame];
        }
        for( i=2; i >= 0; i-- )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"robot_hurt_%02d.png", i]];
            [hurtFrames addObject:frame];
        }
        CCAnimation *hurtAnimation = [CCAnimation animationWithSpriteFrames:[hurtFrames getNSArray] delay:1.0/24.0];
        self.hurtAction = [CCSequence actions:[CCAnimate actionWithAnimation:hurtAnimation], [CCCallFunc actionWithTarget:self selector:@selector(idle)], nil];
        
        //=== KNOCKOUT ANIMATION ===//
        CCArray *knockoutFrames = [CCArray arrayWithCapacity:5];
        for( i=0; i < 5; i++ )
        {
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"robot_knockout_%02d.png", i]];
            [knockoutFrames addObject:frame];
        }
        CCAnimation *knockoutAnimation = [CCAnimation animationWithSpriteFrames:[knockoutFrames getNSArray] delay:1.0/24.0];
        self.knockedOutAction = [CCSequence actions:[CCAnimate actionWithAnimation:knockoutAnimation], [CCBlink actionWithDuration:2.0 blinks:10.0], [CCCallFunc actionWithTarget:self selector:@selector(dead)], nil];
    }

    self.damage = 5.0;
    self.nextDecisionTime = 0.0f;
    
    self.centerToBottom = 39.0;
    self.centerToSides = 29.0;
    
    self.hitPoints = 100.0;
    self.damage = 2.0;
    self.walkSpeed = 80;
    
    self.hitBox = [self createBoundingBoxWithOrigin:ccp(-self.centerToSides, -self.centerToBottom) size:CGSizeMake(self.centerToSides * 2, self.centerToBottom * 2)];
    self.attackBox = [self createBoundingBoxWithOrigin:ccp(self.centerToSides, -5) size:CGSizeMake(25, 20)];
    
    return self;
}

- (void) makeDecisionWithHero:(Hero *)hero andIsOnScreen:(BOOL)isOnScreen
{                                                                                             //don't do anything if...
    if( hero.actionState == kActionStateDead || hero.actionState == kActionStateKnockedOut || //if the hero is dead or knocked out
        self.actionState == kActionStateDead || self.actionState == kActionStateKnockedOut )  //if we are dead or knocked out
        return;
    
    //check if we are close enough to attack the hero
    if( fabsf(hero.position.y - self.position.y) < 10 &&
       (fabsf(hero.position.x - self.position.x) < self.hitBox.original.size.width ) )
    {
        int frequency = 30;
        if( random_range(0, frequency) % frequency == 0 )
        {
            if( hero.position.x < self.position.x )
                self.scaleX = -1.0;
            else
                self.scaleX = 1.0;
            
            [self attack];
            if( CGRectIntersectsRect(hero.hitBox.actual, self.attackBox.actual) )
            {
                [hero hurtWithDamage:self.damage];
            }
        }
        
        self.desiredPosition = self.position; //if we are attacking don't move
    }
    else if( isOnScreen )
    {
        //walk towards the hero
        CGPoint moveDirection = ccpNormalize(ccpSub(hero.position, self.position));
        [self walkWithDirection:moveDirection];

    }
    else
        [self idle];
}

-(void)dead
{
    [super dead];
    self.visible = NO;
}

@end
