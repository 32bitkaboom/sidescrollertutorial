//
//  ActionSprite.m
//  PompaDroid
//
//  Created by John Watson on 12/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ActionSprite.h"

@implementation ActionSprite

-(void)update:(ccTime)dt {
    if (_actionState == kActionStateWalk)
    {
        _desiredPosition = ccpAdd(position_, ccpMult(_velocity, dt));
    }
}

-(BoundingBox) createBoundingBoxWithOrigin:(CGPoint)origin size:(CGSize)size
{
    BoundingBox boundingBox;
    boundingBox.original.origin = origin;
    boundingBox.original.size = size;
    
    boundingBox.actual.origin = ccpAdd(self.position, boundingBox.original.origin);
    boundingBox.actual.size = size;
    
    return boundingBox;
}

-(void)transformBoxes {
    _hitBox.actual.origin = ccpAdd(self.position, ccp(self.hitBox.original.origin.x * self.scaleX, self.hitBox.original.origin.y * self.scaleY));
    _hitBox.actual.size = CGSizeMake(self.hitBox.original.size.width * self.scaleX, self.hitBox.original.size.height * self.scaleY);
    
    _attackBox.actual.origin = ccpAdd(self.position, ccp(self.attackBox.original.origin.x * scaleX_, self.attackBox.original.origin.y * self.scaleY));
    _attackBox.actual.size = CGSizeMake(self.attackBox.original.size.width * self.scaleX, self.attackBox.original.size.height * self.scaleY);
}


-(void)idle
{
    if( self.actionState != kActionStateIdle )
    {
        [self stopAllActions];
        [self runAction:self.idleAction];
        self.actionState = kActionStateIdle;
        self.velocity = CGPointZero;
    }
}

-(void)attack
{
    if( self.actionState == kActionStateIdle || self.actionState == kActionStateWalk )
    {
        [self stopAllActions];
        [self runAction:self.attackAction];
        self.actionState = kActionStateAttack;
    }
}

-(void)hurtWithDamage:(float)damage
{
    if( self.actionState != kActionStateKnockedOut && self.actionState != kActionStateDead )
    {
        [self stopAllActions];
        [self runAction:self.hurtAction];
        self.actionState = kActionStateHurt;
        self.hitPoints -= damage;
        
        if( self.hitPoints <= 0.0 )
        {
            [self knockout];
        }
    }
}

-(void)knockout
{
    [self stopAllActions];
    [self runAction:self.knockedOutAction];
    self.hitPoints = 0.0;
    self.actionState = kActionStateKnockedOut;
}

-(void)dead
{
    [self stopAllActions];
    self.actionState = kActionStateDead;
}

-(void) setPosition:(CGPoint)position
{
    [super setPosition:position];
    [self transformBoxes];
}

-(void)walkWithDirection:(CGPoint)direction
{
    if( self.actionState == kActionStateIdle )
    {
        [self stopAllActions];
        [self runAction:_walkAction];
        self.actionState = kActionStateWalk;
    }
    if( self.actionState == kActionStateWalk )
    {
        self.velocity = ccp((direction.x * self.walkSpeed), (direction.y * self.walkSpeed));
        if( self.velocity.x >= 0 )
            self.scaleX = 1.0;
        else
            self.scaleX = -1.0;
    }
}

@end
