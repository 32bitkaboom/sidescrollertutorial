//
//  ActionSprite.h
//  PompaDroid
//
//  Created by John Watson on 12/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ActionSprite : CCSprite {
    
}

//bounding boxes
@property (nonatomic, assign) BoundingBox hitBox;
@property (nonatomic, assign) BoundingBox attackBox;

//actions
@property (strong, nonatomic) id idleAction;
@property (strong, nonatomic) id attackAction;
@property (strong, nonatomic) id walkAction;
@property (strong, nonatomic) id hurtAction;
@property (strong, nonatomic) id knockedOutAction;

//states
@property (assign, nonatomic) ActionState actionState;

//attributes
@property (assign, nonatomic) float walkSpeed;
@property (assign, nonatomic) float hitPoints;
@property (assign, nonatomic) float damage;

//movement
@property (assign, nonatomic) CGPoint velocity;
@property (assign, nonatomic) CGPoint desiredPosition;

//measurements
@property (assign, nonatomic) float centerToSides;
@property (assign, nonatomic) float centerToBottom;

//action methods
-(void) idle;
-(void) attack;
-(void) hurtWithDamage:(float)damage;
-(void) knockout;
-(void) walkWithDirection:(CGPoint)direction;
-(void) dead;

//scheduled methods
-(void) update:(ccTime)dt;

//bounding box creation helper
-(BoundingBox) createBoundingBoxWithOrigin:(CGPoint)origin size:(CGSize)size;

@end
