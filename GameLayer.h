//
//  GameLayer.h
//  PompaDroid
//
//  Created by John Watson on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "HudLayer.h"

@class Hero, Robot;

@interface GameLayer : CCLayer <SimpleDPadDelegate> {
    CCSpriteBatchNode *_actors;
    Hero *_hero;
}

@property (strong, nonatomic) CCTMXTiledMap *tileMap;
@property (strong, nonatomic) CCArray *robots;
@property int numDroidsAlive;

@end
