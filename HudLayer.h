//
//  HudLayer.h
//  PompaDroid
//
//  Created by John Watson on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#import "SimpleDPad.h"

@interface HudLayer : CCLayer {
    
}

@property (nonatomic, assign) SimpleDPad *dPad;

@end
