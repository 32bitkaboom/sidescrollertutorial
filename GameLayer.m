//
//  GameLayer.m
//  PompaDroid
//
//  Created by John Watson on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameLayer.h"
#import "Hero.h"
#import "Robot.h"
#import "GameScene.h"

#define DEBUG_BOXES
//#define MY_DEBUG

@implementation GameLayer

@synthesize tileMap;

-(id) init
{
    if( self = [super init] )
    {
        [self initTileMap];
        [self scheduleUpdate];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"pd_sprites.plist"];
        _actors = [CCSpriteBatchNode batchNodeWithFile:@"pd_sprites.pvr.ccz"];
        [_actors.texture setAliasTexParameters];
        [self addChild:_actors z:-5];
        
        [self initHero];
        [self initRobots];
        
        self.isTouchEnabled = YES;
    }
    
    return self;
}

-(void)draw
{
    [super draw];
    
#ifdef DEBUG_BOXES
    CCSprite *sprite;
    
    CCARRAY_FOREACH(_actors.children, sprite)
    {
        glLineWidth(2.0f);
        if( [sprite isKindOfClass:[ActionSprite class]] )
        {
            ActionSprite *actionSprite = (ActionSprite *)sprite;
            ccDrawColor4B(0.0f, 0.0f, 255.0f, 1.0f);
            ccDrawRect(ccp(actionSprite.hitBox.actual.origin.x, actionSprite.hitBox.actual.origin.y),
                       ccp(actionSprite.hitBox.actual.origin.x + actionSprite.hitBox.actual.size.width, actionSprite.hitBox.actual.origin.y + actionSprite.hitBox.actual.size.height));
            
            ccDrawColor4B(255.0f, 0.0f, 0.0f, 1.0f);
            ccDrawRect(ccp(actionSprite.attackBox.actual.origin.x, actionSprite.attackBox.actual.origin.y),
                       ccp(actionSprite.attackBox.actual.origin.x + actionSprite.attackBox.actual.size.width, actionSprite.attackBox.actual.origin.y + actionSprite.attackBox.actual.size.height));
        }
    }
#endif
}

#pragma mark Initializers
-(void) initHero
{
    _hero = [Hero node];
    [_actors addChild:_hero z:-4];
    _hero.position = ccp(_hero.centerToSides, 80);
    _hero.desiredPosition = _hero.position;
    [_hero idle];
}

- (void) initRobots
{
#ifdef MY_DEBUG
    int robotCount = 1;
#else
    int robotCount = 50;
#endif
    
    self.robots = [[CCArray alloc] initWithCapacity:50];
    
    for( int x = 0; x < robotCount; x++ )
    {
        Robot *newRobot = [[Robot alloc] init];

        int minX = SCREEN.width + newRobot.centerToSides;
        int maxX = (SCREEN.width * self.tileMap.tileSize.width) - newRobot.centerToSides;
        int minY = newRobot.centerToBottom;
        int maxY = (3 * self.tileMap.tileSize.height) - newRobot.centerToBottom;
        
#ifdef MY_DEBUG
        newRobot.position = ccp(minX, minY);
#else
        newRobot.position = ccp(random_range(minX, maxX), random_range(minY, maxY));
#endif
        newRobot.desiredPosition = newRobot.position;
        newRobot.scaleX = -1;

        [_actors addChild:newRobot z:-5];
        [self.robots addObject:newRobot];
        [newRobot idle];
    }
}

-(void) initTileMap
{
    self.tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"pd_tilemap.tmx"];
    
    for( CCTMXLayer *child in [self.tileMap children] )
    {
        [[child texture] setAliasTexParameters];
    }
    
    [self addChild:self.tileMap z:-6];
}

-(void)dealloc
{
    [super dealloc];
    
    [self unscheduleUpdate];
}

#pragma mark Update Functions
-(void)update:(ccTime)dt
{
    [self updatePositions:dt];
    [self reorderActors];
    [self setViewpointCenter:_hero.position];
    
    if( _hero.actionState == kActionStateDead || self.numDroidsAlive == 0)
    {
        [((GameScene *)self.parent) endGame];
    }
}

-(void)updatePositions:(ccTime)dt {
    [self updateHeroPosition:dt];
    [self updateAllDroids:dt];
}

- (void)updateHeroPosition:(ccTime)dt
{
    [_hero update:dt];
    
    float posX = MIN(self.tileMap.mapSize.width * self.tileMap.tileSize.width - _hero.centerToSides, MAX(_hero.centerToSides, _hero.desiredPosition.x));
    float posY = MIN(3 * self.tileMap.tileSize.height + _hero.centerToBottom, MAX(_hero.centerToBottom, _hero.desiredPosition.y));
    
    _hero.position = ccp(posX, posY);
}

- (void)updateRobotPosition:(Robot *)robot withTimeDelta:(ccTime)dt
{
    [robot update:dt];
    
    float posX = MIN(self.tileMap.mapSize.width * self.tileMap.tileSize.width - robot.centerToSides, MAX(robot.centerToSides, robot.desiredPosition.x));
    float posY = MIN(3 * self.tileMap.tileSize.height + robot.centerToBottom, MAX(robot.centerToBottom, robot.desiredPosition.y));
    
    robot.position = ccp(posX, posY);
}

- (void) updateAllDroids:(ccTime)dt
{
    self.numDroidsAlive = 0;
    
    for (Robot *robot in self.robots) {
        if( robot.hitPoints > 0 )
            self.numDroidsAlive++;
        
        if( CURTIME > robot.nextDecisionTime )
        {
            float distanceSQ = ccpDistanceSQ(robot.position, _hero.position);
            BOOL isOnScreen = (distanceSQ <= SCREEN.width * SCREEN.width);
            
            if( isOnScreen )
                [robot makeDecisionWithHero:_hero andIsOnScreen:isOnScreen];
        }
        
        [self updateRobotPosition:robot withTimeDelta:dt];
    }
}

- (void) reorderActors
{
    ActionSprite *sprite;
    CCARRAY_FOREACH(_actors.children, sprite)
    {
        [_actors reorderChild:sprite z:(self.tileMap.mapSize.height * self.tileMap.tileSize.height) - sprite.position.y];
    }
}

- (void)setViewpointCenter:(CGPoint)position
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    //these keep the user from running off the screen
    int x = MAX(position.x, winSize.width / 2);
    int y = MAX(position.y, winSize.height / 2);
    
    //these keep the user centered in the screen
    x = MIN(x, (self.tileMap.mapSize.width * self.tileMap.tileSize.width) - winSize.width / 2);
    y = MIN(y, (self.tileMap.mapSize.height * self.tileMap.tileSize.height) - winSize.height / 2);
    
    CGPoint actualPosition = ccp(x, y);
    
    CGPoint centerOfView = ccp(winSize.width/2, winSize.height/2);
    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);
    self.position = viewPoint;
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_hero attack];
    
    if( _hero.actionState == kActionStateAttack )
    {
        Robot *robot;
        CCARRAY_FOREACH(self.robots, robot)
        {
            if( robot.actionState != kActionStateKnockedOut )
            {
                if( fabsf(_hero.position.y - robot.position.y) < 10 )
                {
                    if( CGRectIntersectsRect(_hero.attackBox.actual, robot.hitBox.actual) )
                    {
                        [robot hurtWithDamage:_hero.damage];
                    }
                }
            }
        }
    }
}

-(void)simpleDPadTouchEnded:(SimpleDPad *)simpleDPad
{
    if( _hero.actionState == kActionStateWalk )
        [_hero idle];
}

-(void)simpleDPad:(SimpleDPad *)simpleDPad isHoldingDirection:(CGPoint)direction
{
//    NSLog(@"Is Holding Direction");
    [_hero walkWithDirection:direction];
}

-(void)simpleDPad:(SimpleDPad *)simpleDPad didChangeDirectionTo:(CGPoint)direction
{
//    NSLog(@"Did Change Direction To");
    if( _hero.actionState == kActionStateWalk )
        [_hero walkWithDirection:direction];
}

@end
