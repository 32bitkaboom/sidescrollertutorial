//
//  HudLayer.m
//  PompaDroid
//
//  Created by John Watson on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HudLayer.h"


@implementation HudLayer

-(id)init
{
    if( self = [super init] )
    {
        self.dPad = [SimpleDPad dPadWithFile:@"pd_dpad.png" radius:64];
        self.dPad.position = ccp(64.0, 64.0);
        self.dPad.opacity = 100;
        [self addChild:self.dPad];
    }
    
    return self;
}

@end
